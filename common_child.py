import sys

def commonChild(s1, s2):
    # Complete this function
    dynamic_matrix = [ [0]*(len(s2)+1) for _ in range(len(s1)+1) ]
    
    for i in range(1,len(s1)+1): 
        for j in range(1,len(s2)+1):
            if s1[i-1] == s2[j-1]:
                dynamic_matrix[i][j] += 1 + dynamic_matrix[i-1][j-1]
            else:
                dynamic_matrix[i][j] += max(dynamic_matrix[i-1][j], dynamic_matrix[i][j-1])
    
    return dynamic_matrix[len(s1)][len(s2)]
    

s1 = input().strip()
s2 = input().strip()
result = commonChild(s1, s2)
print(result)