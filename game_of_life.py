class Solution(object):
    def check_neighbours(self, board, i, j):
        count_live = 0
        count_dead = 0
        
        if i-1 >= 0 and j - 1 >= 0:
            if board[i-1][j-1] == 1 or board[i-1][j-1] == 'X':
                count_live += 1
            
        
        if i-1 >= 0:
            if board[i-1][j] == 1 or board[i-1][j] == 'X':
                count_live += 1
            
                
        if i-1 >= 0 and j + 1 < len(board[0]):
            if board[i-1][j+1] == 1 or board[i-1][j+1] == 'X':
                count_live += 1
            

        if j - 1 >= 0:
            if board[i][j-1] == 1 or board[i][j-1] == 'X':
                count_live += 1
           
                
        if j + 1 < len(board[0]):
            if board[i][j+1] == 1 or board[i][j+1] == 'X':
                count_live += 1
            
                
        if i+1 < len(board) and j - 1 >= 0:
            if board[i+1][j-1] == 1 or board[i+1][j-1] == 'X':
                count_live += 1
            
                
        if i+1 < len(board):
            if board[i+1][j] == 1 or board[i+1][j] == 'X':
                count_live += 1
            
                
        if i+1 < len(board) and j + 1 < len(board[0]):
            if board[i+1][j+1] == 1 or board[i+1][j+1] == 'X':
                count_live += 1
            
        
        if board[i][j] == 0:
            if count_live == 3:
                board[i][j] = 'Y'
            
        elif board[i][j] == 1:
            if count_live < 2 or count_live > 3:
                board[i][j] = 'X'
                
        return board[i][j]
                
        
    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        
        for i in range(len(board)):
            for j in range(len(board[0])):
                board[i][j] = self.check_neighbours(board, i, j)  
    
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] == 'X':
                    board[i][j] = 0
                elif board[i][j] == 'Y':
                    board[i][j] = 1
                    