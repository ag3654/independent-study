#!/bin/python3

import sys

def roadsAndLibraries(n, c_lib, c_road, cities):
    # Complete this function
    if c_lib <= c_road:
        return c_lib * n

    roads = {}
    visited = set()
    city = set()


    for i in range(1, n + 1):
        city.add(i)
        roads[i] = set()

    for road in cities:
        roads[road[0]].add(road[1])
        roads[road[1]].add(road[0])

    components = 0
    stack = set()
    count = 0

    while len(visited) != n:
        if len(stack) == 0:
            stack.add(city.pop())
            components += 1

        node = stack.pop()
        count += 1

        if node not in visited:
            visited.add(node)
            for neighbours in roads[node]:
                if neighbours not in visited:
                    if neighbours not in stack:
                        stack.add(neighbours)
                if neighbours in city:
                    city.remove(neighbours)

    return (c_lib * components) + (c_road * (len(visited) - components))


if __name__ == "__main__":
    q = int(input().strip())
    for a0 in range(q):
        n, m, c_lib, c_road = input().strip().split(' ')
        n, m, c_lib, c_road = [int(n), int(m), int(c_lib), int(c_road)]
        cities = []
        for cities_i in range(m):
           cities_t = [int(cities_temp) for cities_temp in input().strip().split(' ')]
           cities.append(cities_t)
        result = roadsAndLibraries(n, c_lib, c_road, cities)
        print(result)
