class Solution:
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        stack = []
        best_count = 0
        stack.append(-1)
        for index in range(len(s)):
            if s[index] == '(':
                stack.append(index)
            else:
                stack.pop()
                if len(stack) == 0:
                    stack.append(index)
                else:
                    best_count = max(best_count, index - stack[-1])

        return best_count