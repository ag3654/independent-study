#!/bin/python3

import sys

def count_anagrams(strings):
    count = 0
    
    occurance = {}
    
    for element in strings:
        sorted_string = ''.join(sorted(element))
        if sorted_string in occurance:
            occurance[sorted_string] += 1
        else:
            occurance[sorted_string] = 1
                 
    
    for key in occurance:
        if occurance[key] > 1:
            count += ((occurance[key]) * (occurance[key] - 1)) // 2
            
    return count

def sherlockAndAnagrams(s):
    # Complete this function
    
    sub_strings = []
    equal_length_substrings = {}
    count = 0
    for i in range(len(s)):
        for j in range(len(s)):
            if s[i:j+1] != '':
                sub_strings.append(s[i:j+1])
    
    for substring in sub_strings:
        if len(substring) in equal_length_substrings:
            equal_length_substrings[len(substring)].append(substring)
        else:
            equal_length_substrings[len(substring)] = [substring]
            
            
    for length in equal_length_substrings:
        count += count_anagrams(equal_length_substrings[length])
    
    return count
            
  
q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = sherlockAndAnagrams(s)
    print(result)
