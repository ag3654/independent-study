class Solution:
    def maximalSquare(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        if matrix == []:
            return 0

        S = [[0 for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
        max = -1
        for i in range(len(matrix)):
            S[i][0] = int(matrix[i][0])

        for i in range(len(matrix[0])):
            S[0][i] = int(matrix[0][i])

        for i in range(1, len(matrix)):
            for j in range(1, len(matrix[0])):
                if matrix[i][j] == "1":
                    S[i][j] = min(S[i - 1][j - 1], S[i][j - 1], S[i - 1][j]) + 1

        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if S[i][j] > max:
                    max = S[i][j]
        
        return max*max
