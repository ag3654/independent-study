class Solution:
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        stack = []
        open_brackets = {'(', '{', '['}
        mapping = {')': '(',
                   '}': '{',
                   ']': '['}
    
        for literal in s:
            if literal in open_brackets:
                stack.append(literal)
            else:
                if len(stack) == 0:
                    return False
                if mapping[literal] != (stack.pop()):
                    return False
        
        return len(stack) == 0