#!/bin/python3


def recursive_dfs(node, graph):
    if node not in graph:
        return 0
    else:
        sum = 0
        for child in graph[node]:
            sum += 1 + recursive_dfs(child, graph)

    return sum


def count_paths(node, graph):
    count = 0

    for key in graph:
        no_of_children = recursive_dfs(key, graph) + 1
        if no_of_children % 2 == 0 and no_of_children != 0:
            count += 1

    return count


if __name__ == '__main__':
    tree_nodes, tree_edges = map(int, input().split())

    tree_from = [0] * tree_edges
    tree_to = [0] * tree_edges

    for tree_itr in range(tree_edges):
        tree_from[tree_itr], tree_to[tree_itr] = map(int, input().split())

    #
    # Write your code here.
    #

    nodes = set()
    graph = {}


    for i in range(1, tree_nodes + 1):
        nodes.add(i)


    for i in range(tree_edges):
        if tree_to[i] in graph:
            graph[tree_to[i]].add(tree_from[i])
        else:
            graph[tree_to[i]] = set()
            graph[tree_to[i]].add(tree_from[i])


    print(count_paths(1, graph) - 1)