#!/bin/python3

import os
import sys
import math

#
# Complete the componentsInGraph function below.
#
def componentsInGraph(gb):
    #
    # Write your code here.
    #
    nodes = {}
    visited = set()
    vertices = set()

    for edge in gb:
        if edge[0] not in nodes:
            nodes[edge[0]] = set()
            vertices.add(edge[0])
            nodes[edge[0]].add(edge[1])
        else:
            nodes[edge[0]].add(edge[1])

        if edge[1] not in nodes:
            nodes[edge[1]] = set()
            vertices.add(edge[1])
            nodes[edge[1]].add(edge[0])
        else:
            nodes[edge[1]].add(edge[0])

    no_of_nodes = len(nodes)
    stack = set()
    count = 0
    max_size = 0
    min_size = math.inf


    while len(visited) != no_of_nodes:
        if len(stack) == 0:
            stack.add(vertices.pop())
            if count > max_size:
                max_size = count
            if count < min_size and count > 1:
                min_size = count
            count = 0

        curr = stack.pop()
        count += 1

        if curr not in visited:
            visited.add(curr)
            for neighbours in nodes[curr]:
                if neighbours not in visited:
                    if neighbours not in stack:
                        stack.add(neighbours)

    if count > max_size:
        max_size = count
    if count < min_size and count > 1:
        min_size = count
                        
    return min_size, max_size
        
    

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    gb = []

    for _ in range(n):
        gb.append(list(map(int, input().rstrip().split())))

    result = componentsInGraph(gb)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
