#!/bin/python3

def passwordCracker(password, attempt):
    # Complete this function
    S = [0 for _ in range(len(attempt))]
    backtrack = [None for _ in range(len(attempt))]

    for i in range(len(S)):
        last = attempt[i]
        for word in password:
            if len(word) <= i + 1:
                if word[-1] == last:
                    if attempt[i - len(word) + 1:i + 1] == word:
                        if S[i - len(word)] == 1 or len(word) == len(S[:i + 1]):
                            S[i] = 1
                            backtrack[i] = word
                            break

    if S[-1] == 0:
        return "WRONG PASSWORD"

    backtrack.reverse()

    answer = []

    i = 0
    while i < len(backtrack):
        answer.append(backtrack[i])
        i += len(backtrack[i])

    answer.reverse()
    s = ""
    for word in answer:
        s += word + " "

    return s[:-1]


if __name__ == "__main__":
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        password = input().strip().split(' ')
        attempt = input().strip()
        result = passwordCracker(password, attempt)
        print(result)
