class Solution:
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        if height == []:
            return 0
        
        left = []
        right = []
        ans = []
        max_left = height[0]
        max_right = height[-1]

        for index in range(len(height)):
            if height[index] > max_left:
                max_left = height[index]

            left.append(max_left)

        for index in range(len(height)-1, -1, -1):
            if height[index] > max_right:
                max_right = height[index]

            right.append(max_right)

        right.reverse()

        for index in range(len(height)):
            temp = min(left[index], right[index]) - height[index]
            ans.append(temp)

        sum = 0
        for value in ans:
            sum += value
        
        return sum