import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    public static int countPrimes(int n){
        boolean[] numbers = new boolean[n + 1];
        int count = 0;
        for(int i= 2; i <= n; i++){
            if(numbers[i] == false){
                count++;
                for(int j = i; j <= n; j+= i){
                    numbers[j] = true;
                }
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int g = in.nextInt();
        for(int a0 = 0; a0 < g; a0++){
            int n = in.nextInt();
            // your code goes here
            
            int count = countPrimes(n);
            
            if(count % 2 == 0){
                System.out.println("Bob");
            }
            else{
                System.out.println("Alice");
            }
        }
    }
}