#!/bin/python3

import sys

def gridlandMetro(n, m, k, track):
    count_lamps = 0
    dict_tracks = {}

    for element in track:
        if element[0] in dict_tracks:
            dict_tracks[element[0]] += [element[1:]]
        else:
            dict_tracks[element[0]] = [element[1:]]

    for key in dict_tracks:
        local_count = 0
        dict_tracks[key] = sorted(dict_tracks[key])
        
        if len(dict_tracks[key]) == 1:
            count_lamps += dict_tracks[key][0][1] - dict_tracks[key][0][0] + 1


        else:
            stack = []
            stack.append(dict_tracks[key][0])
            for index in range(1, len(dict_tracks[key])):
                if dict_tracks[key][index][0] > stack[-1][1]:
                    stack.append(dict_tracks[key][index])
                else:
                    temp = stack.pop()
                    start = min(temp[0], dict_tracks[key][index][0])
                    end = max(temp[1], dict_tracks[key][index][1])
                    stack.append([start, end])

            for intervals in stack:
                local_count += intervals[1] - intervals[0] + 1

        count_lamps += local_count

    return m * n - count_lamps


if __name__ == "__main__":
    n, m, k = input().strip().split(' ')
    n, m, k = [int(n), int(m), int(k)]
    track = []
    for track_i in range(k):
       track_t = [int(track_temp) for track_temp in input().strip().split(' ')]
       track.append(track_t)
    result = gridlandMetro(n, m, k, track)
    print(result)
