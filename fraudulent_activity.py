#!/bin/python3

import sys

def find_median(count_array, median_index, isEven):
    current_count = 0
    if isEven == False:
        for i in range(len(count_array)):
            current_count += count_array[i]
            if current_count >= median_index:
                return i

    else:
        median = 0
        for i in range(len(count_array)):
            current_count += count_array[i]
            if current_count >= median_index:
                if count_array[i] >= 2:
                    return i
                else:
                    median += i
                    for j in range(median - 1, -1, -1):
                        if count_array[j] != 0:
                            median += j
                            return median / 2
            elif current_count < median_index:
                if current_count == median_index - 1:
                    median += i
                    for j in range(median + 1, len(count_array)):
                        if count_array[j] != 0:
                            median += j
                            return median / 2

def activityNotifications(expenditure, d):
    # Complete this function
    notifications = 0
    if d % 2 == 0:
        isEven = True
    else:
        isEven = False
    median_index = d // 2 + 1

    count_array = [0] * 200

    for i in range(d):
        count_array[expenditure[i]] += 1


    start = 0
    for i in range(d, len(expenditure)):
        median = find_median(count_array, median_index, isEven)
        
        if median * 2 <= expenditure[i]:
            notifications += 1

        count_array[expenditure[start]] -= 1
        start += 1
        count_array[expenditure[i]] += 1

    return notifications


if __name__ == "__main__":
    n, d = input().strip().split(' ')
    n, d = [int(n), int(d)]
    expenditure = list(map(int, input().strip().split(' ')))
    result = activityNotifications(expenditure, d)
    print(result)
