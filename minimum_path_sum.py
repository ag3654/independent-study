class Solution(object):
    
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])
        D = [[0 for i in range(n)] for j in range(m)]
            
        D[0][0] = grid[0][0]
        for i in range(1, n):
            D[0][i] = D[0][i - 1] + grid[0][i]
            
        for i in range(1, m):
            D[i][0] = D[i-1][0] + grid[i][0]
            
            
        for i in range(1, m):
            for j in range(1, n):
                D[i][j] = grid[i][j] + min(D[i-1][j], D[i][j-1])
        
        return D[m-1][n-1]