#!/bin/python3

import sys

def journeyToMoon(n, astronaut):
    id = set()
    for i in range(n):
        id.add(i)

    graph = {}
    nodes = set()
    visited = set()

    for pair in astronaut:
        if pair[0] not in nodes:
            nodes.add(pair[0])
        if pair[1] not in nodes:
            nodes.add(pair[1])

        if pair[0] in graph:
            graph[pair[0]].add(pair[1])
        else:
            graph[pair[0]] = set()
            graph[pair[0]].add(pair[1])

        if pair[1] in graph:
            graph[pair[1]].add(pair[0])
        else:
            graph[pair[1]] = set()
            graph[pair[1]].add(pair[0])

    stack = set()
    components = []
    temp = []


    count = len(nodes)
    while len(visited) != count:
        if len(stack) == 0:
            stack.add(nodes.pop())
            components.append(temp)
            temp = []

        curr = stack.pop()
        temp.append(curr)
        if curr in id:
            id.remove(curr)

        if curr not in visited:
            visited.add(curr)
            for neighbours in graph[curr]:
                if neighbours in id:
                    id.remove(neighbours)
                if neighbours not in visited:
                    if neighbours not in stack:
                        stack.add(neighbours)

                    if neighbours in nodes:
                        nodes.remove(neighbours)
    components.append(temp)

    for ids in id:
        components.append([ids])

    components = components[1:]
    count_list = []
    
    for comp in components:
        count_list.append(len(comp))

    sum = 0
    answer = 0
    for counts in count_list:
        answer += sum * counts
        sum += counts

    return answer


if __name__ == "__main__":
    n, p = input().strip().split(' ')
    n, p = [int(n), int(p)]
    astronaut = []
    for astronaut_i in range(p):
       astronaut_t = [int(astronaut_temp) for astronaut_temp in input().strip().split(' ')]
       astronaut.append(astronaut_t)
    result = journeyToMoon(n, astronaut)
    print(result)