#!/bin/python3

import sys

def getWays(n, c):
    # Complete this function
    deno = [0 for i in range(n + 1)]

    deno[0] = 1

    for coin in c:
        for i in range(1, len(deno)):
            if i >= coin:
                deno[i] += deno[i - coin]

    print(deno[n])
    
    
n, m = input().strip().split(' ')
n, m = [int(n), int(m)]
c = list(map(int, input().strip().split(' ')))
# Print the number of ways of making change for 'n' units using coins having the values given by 'c'
ways = getWays(n, c)
