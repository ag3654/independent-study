#!/bin/python3

import sys

def nonDivisibleSubset(k, arr):
    # Complete this function
    largest_subset_size = 0
    remainders = {}
    for number in arr:
        if number%k not in remainders:
            remainders[number%k] = 1
        else:
            remainders[number%k] += 1
    
    
    for key in remainders:
        if k - key in remainders:
            if key == k - key:
                largest_subset_size += 1
            else:
                largest_subset_size += max(remainders[key], remainders[k - key]) / 2
        elif key == 0:
            largest_subset_size += 1
        else:
            largest_subset_size += remainders[key]
             
    return int(largest_subset_size)
    

if __name__ == "__main__":
    n, k = input().strip().split(' ')
    n, k = [int(n), int(k)]
    arr = list(map(int, input().strip().split(' ')))
    result = nonDivisibleSubset(k, arr)
    print(result)
