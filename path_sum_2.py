# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def check_path(self, root, local_path, sum, paths):
        # checking for the leaf node
        if root.left == None and root.right == None and root.val == sum:
            local_path.append(root.val)
            paths.append(local_path)
        
        else:
            if root.left != None:
                self.check_path(root.left, local_path + [root.val], sum-root.val, paths)
            if root.right != None:
                self.check_path(root.right, local_path + [root.val], sum-root.val, paths)
        
        return paths
  
    
    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: List[List[int]]
        """
        if root == None:
            return []
        
        paths = self.check_path(root, [], sum, [])
        return paths